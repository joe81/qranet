#include "CRemoteServersModel.h"
//-----------------------------------------------------------------------------
CRemoteServersModel::CRemoteServersModel(QObject *parent) :
    QAbstractListModel(parent){
  fDataStorage = NULL;
}
//-----------------------------------------------------------------------------
int CRemoteServersModel::rowCount(const QModelIndex &parent) const{
    quint32 Result = 0;
    if(fDataStorage)
        Result = fDataStorage->RemoteServers.size();
    return Result;
}
//-----------------------------------------------------------------------------
QVariant CRemoteServersModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid() || fDataStorage == NULL) {
        return QVariant();
    }
    if(role == Qt::DisplayRole){
        QMap<quint32,CRemoteServer* >::iterator i = fDataStorage->RemoteServers.begin()+index.row();
        if(i!= fDataStorage->RemoteServers.end()){
            CRemoteServer *lserver = *i;
            if(lserver!=NULL)
                return  QVariant(QString("%1 (%2)").arg(lserver->Name).arg(QHostAddress(lserver->IP).toString()));
        }
    }
    return  QVariant();
}
//-----------------------------------------------------------------------------
QVariant CRemoteServersModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal){
        return  tr("Удаленные сервера");
    }
    return  QVariant();
}
//-----------------------------------------------------------------------------
void CRemoteServersModel::SetStorage(CRaNetServer* adata){
    if(fDataStorage!=NULL){
        disconnect(fDataStorage,SIGNAL(RemoteServerDiscover(quint32)),this,SLOT(Discover(quint32)));
        disconnect(fDataStorage,SIGNAL(RemoteServerLost(quint32)),this,SLOT(Lost(quint32)));
    }
    fDataStorage = adata;
    if(fDataStorage!=NULL){
        connect(fDataStorage,SIGNAL(RemoteServerDiscover(quint32)),this,SLOT(Discover(quint32)));
        connect(fDataStorage,SIGNAL(RemoteServerLost(quint32)),this,SLOT(Lost(quint32)));
    }
}
//-----------------------------------------------------------------------------
void CRemoteServersModel::Discover(quint32 aip){
    beginInsertRows(QModelIndex(),0,rowCount(QModelIndex()));
    endInsertRows();
}
//-----------------------------------------------------------------------------
void CRemoteServersModel::Lost(quint32 aip)
{
    beginInsertRows(QModelIndex(),0,rowCount(QModelIndex()));
    endInsertRows();

}
//-----------------------------------------------------------------------------
