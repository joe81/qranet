#ifndef QRANETWINDOW_H
#define QRANETWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>
#include <QLabel>

#include "CRaNetServer.h"
#include "CRemoteServersModel.h"


class QRanetWindow : public QMainWindow
{
    Q_OBJECT
private:
    CRaNetServer  *fRanetThread;
    QComboBox   *fSelectSubNet;
    QLineEdit   *fPortEdit;
    QLineEdit   *fMemoryName;
    QPushButton *fStartButton;
    QLabel      *fModelCountLabel;
    quint16     fModelCount;
    CRemoteServersModel *fRemoteModel;

public:
    QRanetWindow(QWidget *parent = 0);
    ~QRanetWindow();
public slots:
    void    onStartStop();
};

#endif // QRANETWINDOW_H
