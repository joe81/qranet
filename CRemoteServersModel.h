#ifndef CREMOTESERVERSMODEL_H
#define CREMOTESERVERSMODEL_H

#include <QAbstractListModel>
#include "CRaNetServer.h"
#include <QMap>

//-----------------------------------------------------------------------------
class CRemoteServersModel : public QAbstractListModel
{
    Q_OBJECT
private:
    CRaNetServer    *fDataStorage;
public:
    explicit CRemoteServersModel(QObject *parent = 0);

signals:

public slots:


    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    void    SetStorage(CRaNetServer* adata);
private slots:
    void    Discover(quint32 aip);
    void    Lost(quint32 aip);

};
//-----------------------------------------------------------------------------

#endif // CREMOTESERVERSMODEL_H
