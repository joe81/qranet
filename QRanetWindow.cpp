#include "QRanetWindow.h"
#include <QVBoxLayout>
#include <QSettings>
#include <QSpacerItem>
#include <QListView>
#include <QTabWidget>


QRanetWindow::QRanetWindow(QWidget *parent)
    : QMainWindow(parent)
{    

    // Создаю центральный виджет
    QWidget *lcentral = new QWidget();
    QTabWidget *ltab = new QTabWidget(lcentral);


    QVBoxLayout *lcentrallayout = new QVBoxLayout(lcentral);



    // Создаю разметку центрального виджета (основная разметка)

    QWidget *lSettingsTab = new QWidget;
    QVBoxLayout *llayout = new QVBoxLayout(lSettingsTab);
    // --- Создание виджетов

    fModelCountLabel = new QLabel(lSettingsTab);
    // ------   Выбор рабочей подсети
    fSelectSubNet = new QComboBox(lSettingsTab);
    // ------   Выбор порта
    fPortEdit = new QLineEdit(lSettingsTab);
    // ------
    fStartButton = new QPushButton(lSettingsTab);
    fStartButton->setText(tr("Запуск"));
    // ------
    fMemoryName = new QLineEdit(lSettingsTab);
    //---


    QWidget *lRemoteTab = new QWidget;
    QVBoxLayout *lremotelayout = new QVBoxLayout(lRemoteTab);

    CRemoteServersModel *lmodel =   new CRemoteServersModel(lRemoteTab);

    QListView *lremotelist = new QListView(lRemoteTab);
    lremotelist->setModel(lmodel);

    lremotelayout->addWidget(lremotelist);

    // ---
    llayout->addWidget(fSelectSubNet);
    llayout->addWidget(fPortEdit);
    llayout->addWidget(fModelCountLabel);
    llayout->addWidget(fMemoryName);

    ltab->addTab(lSettingsTab,tr("Настройка"));
    ltab->addTab(lRemoteTab,tr("Удаленные сервера"));
    lcentrallayout->addWidget(ltab);
    lcentrallayout->addWidget(fStartButton);

    setCentralWidget(lcentral);

    // ---- Инициализация
    connect(fStartButton,SIGNAL(clicked()),this,SLOT(onStartStop()));
    //  ---------   Инициализация CRaNetServer
    fRanetThread = new CRaNetServer("RaNET.ini");
    QList<QNetworkAddressEntry> lentrys = fRanetThread->NetworkEntrys();
    foreach (QNetworkAddressEntry lentry, lentrys) {
        fSelectSubNet->addItem(lentry.ip().toString());
    }
    fSelectSubNet->setCurrentIndex(
                fSelectSubNet->findText(
                    fRanetThread->IPString()));

    fPortEdit->setText(QString::number(fRanetThread->BasePort()));
    fMemoryName->setText(fRanetThread->MemoryName());
    fModelCountLabel->setText(tr("Кол-во моделей: ")+QString::number(fRanetThread->ModelCount()));
    lmodel->SetStorage(fRanetThread);


}
//-----------------------------------------------------------------------------
QRanetWindow::~QRanetWindow(){

        delete fRanetThread;

/*    QSettings lsettings("RaNET.ini",QSettings::IniFormat);
    lsettings.beginGroup("Identification");
    lsettings.setValue("IPAddress",fSelectSubNet->currentText());
    lsettings.setValue("BasePort",fPortEdit->text());
    lsettings.setValue("CommonMemoryName",fMemoryName->text());
    lsettings.endGroup();*/
}
//-----------------------------------------------------------------------------
void    QRanetWindow::onStartStop(){
    if(!fRanetThread->property("Running").toBool()){
        fRanetThread->setSelectEntry(QHostAddress(fSelectSubNet->currentText()).toIPv4Address());

        bool    ok = false;
        quint16 lport = fPortEdit->text().toInt(&ok);
        if(ok)
            fRanetThread->setBasePort(lport);

        fRanetThread->start(QList<quint16>()<<1<<2<<3);

        fSelectSubNet->setEnabled(false);
        fPortEdit->setEnabled(false);
        fMemoryName->setEnabled(false);
        fStartButton->setText(tr("Закрыть"));
        fStartButton->setCheckable(true);
        fStartButton->setChecked(true);
    } else {
        close();
    }



}
