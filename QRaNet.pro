#-------------------------------------------------
#
# Project created by QtCreator 2014-06-10T10:08:25
#
#-------------------------------------------------

QT       += core gui


include(RaNetModules/QRaNet.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QRaNet
TEMPLATE = app


SOURCES += main.cpp\
        QRanetWindow.cpp \
    CRemoteServersModel.cpp

HEADERS  += QRanetWindow.h \
    CRemoteServersModel.h
