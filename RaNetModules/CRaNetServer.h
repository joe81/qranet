﻿#ifndef CRANETSERVERTHREAD_H
#define CRANETSERVERTHREAD_H

#include <QMutex>
#include <QUdpSocket>
#include <QTcpServer>
#include <QTimer>
#include <QNetworkInterface>
#include <QNetworkConfigurationManager>
#include "CSharedMemoryWrapper.h"
#include "CRaNetModelData.h"

class  CRemoteServer : public QObject{
    Q_OBJECT
private:

    bool m_Activ;
    bool m_Started;

public:
    Q_PROPERTY(bool Activ READ Activ WRITE setActiv)    
    explicit CRemoteServer(QObject *parent = 0);
    ~CRemoteServer();
    QTimer  ActivityTimer;
    QString Name;
    quint32 IP;
    void    HeartBeat();
    bool    Activ(){
        return  m_Activ;
    }
public slots:
    void    setActiv(bool arg);
signals:
    void    Deactiv(quint32 aip);
private slots:
    void    Timeout(){
        setActiv(false);
    }
};
//-----------------------------------------------------------------------------
class CRaNetServer : public QObject
{
    Q_OBJECT
private:
    CSharedMemoryWrapper    fRanetMemory;
    QUdpSocket              *fBroadCastSocket;
    QTcpServer              *fCommandServer;
    QTimer                  *fSendBroadCastTimer;
    QTimer                  *fUpdateTimer;
    QNetworkAddressEntry    fNetworkEntry;
    quint16                 m_BasePort;
    QList<QNetworkAddressEntry> fEntrys;

    quint16 m_ModelCount;
    QList<CRaNetModelData *>  fListenModel; // Индекс модели с 0
    void    fCreateMemory();
    void    fRefreshInterface();
    bool    m_Running;
    quint32 fSelectedIP;
    QNetworkConfigurationManager    fNetworlConfigurationManager;

public:
    explicit CRaNetServer(QObject *parent = 0);
    CRaNetServer(QString afilename, QObject *parent = 0);

    ~CRaNetServer();
    QMap <quint32,CRemoteServer*> RemoteServers;

    Q_PROPERTY(quint16 BasePort READ BasePort WRITE setBasePort)
    Q_PROPERTY(quint32  IP READ IP WRITE setSelectEntry)
    Q_PROPERTY(quint16 ModelCount READ ModelCount WRITE setModelCount)
    Q_PROPERTY(bool Running MEMBER m_Running)
    Q_PROPERTY(QString MemoryName READ MemoryName WRITE setMemoryName)

    CRaNetModelData*    GetModel(quint16 aIndex);
    void                GetModel(QList<quint16> alist);
    void*               data();
    quint32             size();
    bool                lock();
    bool                unlock();

    quint16 BasePort() const{    return m_BasePort;}    
    quint32 IP() const{ return fNetworkEntry.ip().toIPv4Address();}
    QString IPString() const{ return fNetworkEntry.ip().toString();}
    quint16 ModelCount() const{return m_ModelCount;}
    QString MemoryName()const {return fRanetMemory.nativeKey();}
    QList<QNetworkAddressEntry >    NetworkEntrys();

signals:
    void    RemoteServerDiscover(quint32 address);
    void    RemoteServerLost(quint32 address);

private slots:
    void    processModelSocket();
    void    processPendingDatagrams();
    void    sendBroadcast();    
    void    init();
    void    newconnection();
    void    fNetworkConfigurationChanged(const QNetworkConfiguration & config);
public slots:
    void    DeactivRemoteServer(quint32 aip);
    void    start(QList<quint16> aModelList = QList<quint16>());
    void    startWithAllModel();
    void    stop();
    void    setMemoryName(QString aname);
    void    setBasePort(quint16 arg);
    void    setSelectEntry(quint32 arg);
    void    setModelCount(quint16 arg);
    void    LoadFromIni(QString afilename);


};

#endif // CRANETSERVERTHREAD_H
