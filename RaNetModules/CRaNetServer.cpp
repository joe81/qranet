﻿#include "CRaNetServer.h"
#include <QDebug>
#include <QTime>
#include <QtEndian>
#include <QHostInfo>
#include <QSettings>
#include <QNetworkSession>
#include "PacketsType.h"

#define     RANET_BASE_PORT 4050
#define     REMOTE_SERVER_TIMEOUT   2500
const   QString RanetMemoryName("RaNETFileMappingObject");
//-----------------------------------------------------------------------------
CRemoteServer::CRemoteServer(QObject *parent ) : QObject(parent){
    m_Activ = false;
    ActivityTimer.setSingleShot(true);
    connect(&ActivityTimer,SIGNAL(timeout()),this,SLOT(Timeout()));
}
//-----------------------------------------------------------------------------
CRemoteServer::~CRemoteServer(){
    emit Deactiv(IP);
}
//-----------------------------------------------------------------------------
void CRemoteServer::HeartBeat(){
    m_Activ = true;
    ActivityTimer.start(REMOTE_SERVER_TIMEOUT);
}
//-----------------------------------------------------------------------------
void CRemoteServer::setActiv(bool arg){
    if(m_Activ != arg){
        m_Activ = arg;
        if(!m_Activ)
            emit    Deactiv(IP);
    }
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
CRaNetServer::CRaNetServer(QObject *parent) :
    QObject(parent){
    fBroadCastSocket = NULL;
    m_Running = false;
    setMemoryName(RanetMemoryName);
    fRefreshInterface();
    m_BasePort = RANET_BASE_PORT;
    fListenModel.clear();

    RemoteServers.clear();
    fSendBroadCastTimer = new QTimer(this);

    connect(&fNetworlConfigurationManager,SIGNAL(configurationAdded(QNetworkConfiguration)),this,SLOT(fNetworkConfigurationChanged(QNetworkConfiguration)));
    connect(&fNetworlConfigurationManager,SIGNAL(configurationChanged(QNetworkConfiguration)),this,SLOT(fNetworkConfigurationChanged(QNetworkConfiguration)));
}
//-----------------------------------------------------------------------------
CRaNetServer::CRaNetServer(QString afilename, QObject *parent) :
    QObject(parent){
    fBroadCastSocket = NULL;
    m_Running = false;
//    setMemoryName(RanetMemoryName);
    fRefreshInterface();
    fListenModel.clear();
    LoadFromIni(afilename);

    RemoteServers.clear();
    fSendBroadCastTimer = new QTimer(this);

    connect(&fNetworlConfigurationManager,SIGNAL(configurationAdded(QNetworkConfiguration)),this,SLOT(fNetworkConfigurationChanged(QNetworkConfiguration)));
    connect(&fNetworlConfigurationManager,SIGNAL(configurationChanged(QNetworkConfiguration)),this,SLOT(fNetworkConfigurationChanged(QNetworkConfiguration)));

}
//-----------------------------------------------------------------------------
CRaNetServer::~CRaNetServer(){
    qDebug()<<tr("Закрытие QRanet");
    fSendBroadCastTimer->stop();
    foreach (CRaNetModelData* lmodel, fListenModel){
            lmodel->deleteLater();
    }
    fListenModel.clear();
    fRanetMemory.detach();
}
//-----------------------------------------------------------------------------
void    CRaNetServer::fNetworkConfigurationChanged(const QNetworkConfiguration & /*config*/){
//    QNetworkSession lsession(config);
//    qDebug()<<lsession.interface().humanReadableName()<<lsession.interface().index();
//    foreach (QNetworkAddressEntry lentry, lsession.interface().addressEntries()) {
//        qDebug()<<lentry.ip();
//    }
    if(fNetworkEntry.ip().isNull()){
        fRefreshInterface();
        setSelectEntry(fSelectedIP);
    }
}
//-----------------------------------------------------------------------------
QList<QNetworkAddressEntry >    CRaNetServer::NetworkEntrys(){
    fRefreshInterface();
    return  fEntrys;
}
//-----------------------------------------------------------------------------
void CRaNetServer::LoadFromIni(QString afilename){
    QSettings lsettings(afilename,QSettings::IniFormat);
    bool    ok = false;
    quint16 lport = lsettings.value("Identification/BasePort",RANET_BASE_PORT).toInt(&ok);
    if(ok)
        setBasePort(lport);
    setMemoryName(lsettings.value("Identification/CommonMemoryName",RanetMemoryName).toString());
    QHostAddress    laddr(lsettings.value("Identification/IPAddress","127.0.0.1").toString());
    if(!laddr.isNull())
        setSelectEntry(laddr.toIPv4Address());


    quint16 lmodelcount = lsettings.value("Models/ModelCount",1).toInt(&ok);
    if(ok)
            setModelCount(lmodelcount);

}
//-----------------------------------------------------------------------------
void CRaNetServer::fRefreshInterface(){
    // ---
    fEntrys.clear();
    foreach (QNetworkInterface linterface, QNetworkInterface::allInterfaces()) {
        if(!linterface.flags().testFlag(QNetworkInterface::IsLoopBack) &&
            linterface.flags().testFlag(QNetworkInterface::IsRunning)){
            foreach (QNetworkAddressEntry lentry, linterface.addressEntries()) {
                if(lentry.ip().protocol() == QAbstractSocket::IPv4Protocol)
                    fEntrys.append(lentry);
            }
        }
    }

    qDebug()<<"Обнаружены сетевые интерфесы:";
    foreach (QNetworkAddressEntry lentry, fEntrys)
        qDebug()<<"\t"<<lentry.ip().toString();
}
//-----------------------------------------------------------------------------
void CRaNetServer::setMemoryName(QString aname){
    qDebug()<<QString(tr("Установлено имя общей паямяти %1")).arg(aname);
#if defined(Q_OS_WIN)||defined(Q_OS_ANDROID)
    fRanetMemory.setNativeKey(aname);
#else
    fRanetMemory.setKey(aname);
#endif

}
//-----------------------------------------------------------------------------
void CRaNetServer::setModelCount(quint16 arg) {
    qDebug()<<QString(tr("Установлено кол-во моделей %1")).arg(arg);
    m_ModelCount = arg;
}
//-----------------------------------------------------------------------------
void CRaNetServer::setBasePort(quint16 arg){
    qDebug()<<QString(tr("Установлен базовый порт %1")).arg(arg);
    m_BasePort = arg;
}
//-----------------------------------------------------------------------------
void CRaNetServer::DeactivRemoteServer(quint32 aip){
    if(RemoteServers.count(aip)){
        CRemoteServer *lserver = RemoteServers.take(aip);
        if(lserver){
            emit    RemoteServerLost(lserver->IP);
            qDebug()<<QString(tr("Потерян удаленный сервер %1").arg(QHostAddress(aip).toString()));
            delete lserver;
        }

    }
}
//-----------------------------------------------------------------------------
void CRaNetServer::processPendingDatagrams(){
    while (fBroadCastSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(fBroadCastSocket->pendingDatagramSize());
        QHostAddress laddress;
        quint16 lport;
        fBroadCastSocket->readDatagram(datagram.data(), datagram.size(),&laddress,&lport);
        if(laddress.isInSubnet(fNetworkEntry.ip(),fNetworkEntry.prefixLength()) && laddress!=fNetworkEntry.ip()){
            QMap<quint32, CRemoteServer* >::iterator i = RemoteServers.find(laddress.toIPv4Address());
            CRemoteServer* lremote = NULL;
            TUDPBroadcastPacket *ludpdata = reinterpret_cast <TUDPBroadcastPacket *> (datagram.data());

            if(i == RemoteServers.end()){
                lremote = new CRemoteServer(this);
                lremote->IP = laddress.toIPv4Address();
                lremote->Name = QString(ludpdata->Name);
                RemoteServers.insert(lremote->IP,lremote);
                connect(lremote,SIGNAL(Deactiv(quint32)),this,SLOT(DeactivRemoteServer(quint32)));
                emit RemoteServerDiscover(lremote->IP);
                qDebug()<<QString(tr("Обнаружен удаленный сервер %1").arg(QHostAddress(lremote->IP).toString()));

            }else
                lremote = i.value();
            lremote->HeartBeat();
        }
    }
}
//-----------------------------------------------------------------------------
void CRaNetServer::init(){
    fBroadCastSocket = new QUdpSocket(this);
    fBroadCastSocket->bind(m_BasePort,QAbstractSocket::ShareAddress);
    connect(fBroadCastSocket, SIGNAL(readyRead()),this, SLOT(processPendingDatagrams()));
    //---
    fSendBroadCastTimer->setInterval(500);
    connect(fSendBroadCastTimer,SIGNAL(timeout()),this,SLOT(sendBroadcast()),Qt::QueuedConnection);
    fSendBroadCastTimer->start();

    fCommandServer = new QTcpServer(this);
    connect(fCommandServer,SIGNAL(newConnection()),this,SLOT(newconnection()));
    if(fCommandServer->listen(QHostAddress::Any,m_BasePort+1))
        qDebug()<<QString(tr("TCP сервер слушает порт %1")).arg(fCommandServer->serverPort());
    else
        qDebug()<<QString(tr("TCP сервен не может быть настроен на порт %1. Ошибка %2").arg(fCommandServer->serverPort())).arg(fCommandServer->errorString());

}
//-----------------------------------------------------------------------------
void    CRaNetServer::processModelSocket(){
    QUdpSocket *fsocket =qobject_cast<QUdpSocket*>(sender());
    if(fsocket!=NULL){
        while (fsocket->hasPendingDatagrams()) {
            QByteArray datagram;
            datagram.resize(fsocket->pendingDatagramSize());
            fsocket->readDatagram(datagram.data(), datagram.size());
        }
    }

}
//-----------------------------------------------------------------------------
void    CRaNetServer::newconnection(){
//    qDebug()<<"new connection";
    while(fCommandServer->hasPendingConnections()){
        QTcpSocket* fsock = fCommandServer->nextPendingConnection();        
    }

}
//-----------------------------------------------------------------------------
void    CRaNetServer::GetModel(QList<quint16> alist){
    foreach (quint16 aindex, alist)
        GetModel(aindex);

}
//-----------------------------------------------------------------------------
CRaNetModelData* CRaNetServer::GetModel(quint16 aIndex){
    if(aIndex >= m_ModelCount)
        return NULL;
    foreach (CRaNetModelData* lmodel, fListenModel){
        if(lmodel->Index() == aIndex)
            return  lmodel;
    }    
    qDebug()<<QString(tr("Создание модели %1.")).arg(aIndex);
    CRaNetModelData* lmodel = NULL;    
    lmodel = new CRaNetModelData(aIndex,m_BasePort+10+aIndex,NULL);
    if(!fRanetMemory.isAttached()){
        fRanetMemory.attach(CSharedMemoryWrapper::ReadWrite);
    }
    if(fRanetMemory.isAttached())
        lmodel->setMemory((fRanetMemory.data()+aIndex*MODEL_DEFAULT_SIZE));
    else
        qWarning()<<QString(tr("Память еще не размечена. Модель %1")).arg(aIndex);
    fListenModel.append(lmodel);
    return  lmodel;
}
//-----------------------------------------------------------------------------
void CRaNetServer::stop(){

}
//-----------------------------------------------------------------------------
void* CRaNetServer::data(){
    return  fRanetMemory.data();
}
//-----------------------------------------------------------------------------
quint32 CRaNetServer::size(){
    return  fRanetMemory.size();
}

//-----------------------------------------------------------------------------
void CRaNetServer::fCreateMemory(){
    if(!fRanetMemory.isAttached()){
        if(!fRanetMemory.attach(CSharedMemoryWrapper::ReadWrite)){
            qDebug()<<QString(tr("Создание общей памяти %3 на %1 моделей. общий размер %2 байт.")).arg(m_ModelCount).arg(m_ModelCount * MODEL_DEFAULT_SIZE).arg(fRanetMemory.nativeKey());
            if(fRanetMemory.create(m_ModelCount * MODEL_DEFAULT_SIZE,CSharedMemoryWrapper::ReadWrite))
                memset(fRanetMemory.data(),0,m_ModelCount * MODEL_DEFAULT_SIZE);
            else
                qCritical()<<QString(tr("Ошибка создания общей памяти. %1")).arg(fRanetMemory.errorString());
        }else{
            qDebug()<<QString(tr("Успешное подключение к общей памяти %1")).arg(fRanetMemory.nativeKey());
        }
    }
}
//-----------------------------------------------------------------------------
void CRaNetServer::sendBroadcast(){
    if(fBroadCastSocket!=NULL){
        TUDPBroadcastPacket ldata;
        memset(&ldata,0,sizeof(ldata));
        ldata.Version = 3;

        #ifdef  Q_OS_ANDROID
            strcpy(ldata.Name,"Android");
        #else
            strcpy(ldata.Name,QHostInfo::localHostName().toLatin1().data());
        #endif
        ldata.Address=qToBigEndian(fNetworkEntry.ip().toIPv4Address());
        ldata.ModelCount=m_ModelCount;
        ldata.ModelsExists[0] = 0;
        ldata.ModelsExists[1] = 0;

        fBroadCastSocket->writeDatagram((char*)(&ldata),sizeof(TUDPBroadcastPacket),fNetworkEntry.broadcast(),m_BasePort);
    }

}
//-----------------------------------------------------------------------------
bool CRaNetServer::lock(){
    return  fRanetMemory.lock();
}
//-----------------------------------------------------------------------------
bool CRaNetServer::unlock(){
    return  fRanetMemory.unlock();
}
//-----------------------------------------------------------------------------
void CRaNetServer::setSelectEntry(quint32 arg){
    fSelectedIP = arg;
    foreach (QNetworkAddressEntry laddr, fEntrys){
        if(laddr.prefixLength()<0)
            laddr.setPrefixLength(24);
        if((laddr.ip().toIPv4Address() & laddr.netmask().toIPv4Address())==(fSelectedIP & laddr.netmask().toIPv4Address())){
            fNetworkEntry = laddr;
            qDebug()<<QString(tr("Выбран сетевой интерфейс %1")).arg(fNetworkEntry.ip().toString());
        }
    }
}
//-----------------------------------------------------------------------------
void CRaNetServer::startWithAllModel(){
    QList<quint16>  lmodelindex;
    for(int i=0;i<m_ModelCount;i++)
        lmodelindex.append(i);
    start(lmodelindex);
}
//-----------------------------------------------------------------------------
void CRaNetServer::start(QList<quint16> aModelList){
    if(!m_Running){
        qDebug()<<tr("Запуск сервера.");
        m_Running = true;
        init();
        fCreateMemory();
    }
    GetModel(aModelList);
}
