QT  +=  network

INCLUDEPATH  += $$PWD
SOURCES +=  \
    $$PWD/CRaNetModelData.cpp \
    $$PWD/CRaNetServer.cpp \
    $$PWD/CSharedMemoryWrapper.cpp

HEADERS  += \
    $$PWD/CRaNetModelData.h \
    $$PWD/CRaNetServer.h \
    $$PWD/CSharedMemoryWrapper.h \
    $$PWD/PacketsType.h
