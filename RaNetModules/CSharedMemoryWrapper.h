#ifndef CSHAREDMEMORYWRAPPER_H
#define CSHAREDMEMORYWRAPPER_H


#if !defined(Q_OS_QNX)
#include <QSharedMemory>
#endif
#if defined(Q_OS_ANDROID)
    #include <QMap>
#endif

class CSharedMemoryWrapper
        #if !defined(Q_OS_QNX)
        : public QSharedMemory
        #endif
{
//    Q_OBJECT
public:
//    CSharedMemoryWrapper(QObject *parent=NULL):QObject(parent){}
//    CSharedMemoryWrapper(const QString &key, QObject *parent = 0);
#if defined(Q_OS_QNX)||defined(Q_OS_ANDROID)
    ~CSharedMemoryWrapper();
    bool attach(QSharedMemory::AccessMode mode = ReadWrite);
    bool detach();
    bool create(int size, QSharedMemory::AccessMode mode = ReadWrite);
    void* data() { return m_memory; }
    const void* data() const { return m_memory; }
    bool isAttached() const { return m_attached; }
    void setNativeKey(QString akey){m_key = akey;}
    bool lock();
    bool unlock();
private:
#if defined(Q_OS_ANDROID)
    static QMap<QString,void*>    fMemoryPointerMap;
#endif

    QString m_key;
    int  m_file, m_size;
    bool m_attached;
    void *m_memory;
#endif

};

#endif // CSHAREDMEMORYWRAPPER_H
