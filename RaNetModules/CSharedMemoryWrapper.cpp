#include "CSharedMemoryWrapper.h"


#if defined(Q_OS_ANDROID)
    QMap<QString,void*>    CSharedMemoryWrapper::fMemoryPointerMap;
#endif


#if !defined(Q_OS_QNX)&&!defined(Q_OS_ANDROID)
//CSharedMemoryWrapper::CSharedMemoryWrapper(const QString &key, QObject *parent)
//    : QSharedMemory(key, parent) {
//}
#else
//CSharedMemoryWrapper::CSharedMemoryWrapper(const QString &key, QObject *parent)
//    : m_key(key), m_file(-1), m_attached(false), m_memory(NULL) {
//}

#if defined(Q_OS_QNX)||defined(Q_OS_ANDROID)
CSharedMemoryWrapper::~CSharedMemoryWrapper() {
    detach();
}
#endif
bool CSharedMemoryWrapper::lock(){
    return  true;
}

bool CSharedMemoryWrapper::unlock(){
    return  true;
}

bool CSharedMemoryWrapper::attach(QSharedMemory::AccessMode /*mode*/) {
    #if defined(Q_OS_QNX)
    if(isAttached()) return true;
    if(m_file == -1) return false;
    if((m_memory = mmap(0, m_size, PROT_READ|PROT_WRITE, MAP_SHARED, m_file, 0)) == MAP_FAILED) return false;
    return m_attached = true;
    #endif
    #if defined(Q_OS_ANDROID)
    if(!fMemoryPointerMap.contains(m_key)){
        m_memory = new unsigned char[m_size];
        fMemoryPointerMap[m_key] = m_memory;
    } else
        m_memory = fMemoryPointerMap[m_key];
    return m_attached = true;
    #endif
}

bool CSharedMemoryWrapper::detach(){
    #if defined(Q_OS_ANDROID)
    if(m_memory!=NULL){
        delete m_memory;
        m_memory = NULL;
    }
    m_attached = false;
    #endif
    return  true;
}

bool CSharedMemoryWrapper::create(int size, QSharedMemory::AccessMode /*mode*/) {
    #if defined(Q_OS_QNX)
    if((m_file = shm_open(m_key.toLocal8Bit().data(),O_RDWR|O_CREAT,0777)) == -1) return false;
    ftruncate(m_file, m_size = size);
    return true;
    #endif
    #if defined(Q_OS_ANDROID)
    m_size = size;
    if(!fMemoryPointerMap.contains(m_key)){
        m_memory = new unsigned char[m_size];
        fMemoryPointerMap[m_key] = m_memory;
    } else
        m_memory = fMemoryPointerMap[m_key];
    return  true;
    #endif
}
#endif
