#ifndef CRANETMODELDATA_H
#define CRANETMODELDATA_H

#include <QObject>
#include <QUdpSocket>
#include <QTimer>
#include <QTime>
#define MODEL_DEFAULT_SIZE  65536


class CRaNetModelData : public QObject
{
    Q_OBJECT
private:
    quint16 fIndex;

    void * fMemory;
    QUdpSocket  *fSockect;
    QTimer      *fUpdateTimer;
    QTime       fTimer;
public:
    static  quint32 fPacketFreq;
    explicit CRaNetModelData(quint16 aIndex,quint16 aport,QObject *parent = 0);
    ~CRaNetModelData();
    Q_PROPERTY(quint16 Index READ Index WRITE setIndex)
    Q_PROPERTY(void * Memory READ Memory WRITE setMemory )

    quint16 Index() const
    {
        return fIndex;
    }

    void * Memory() const
    {
        return fMemory;
    }

private slots:
    void    onDataReceive();
    void    UpdateDataModelTimer();

signals:
    void    DataReceive();

public slots:

void setIndex(quint16 arg)
{
    fIndex = arg;
}
void setMemory(void * arg)
{
    fMemory = arg;
}
};

#endif // CRANETMODELDATA_H
