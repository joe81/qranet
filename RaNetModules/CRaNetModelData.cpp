#include "CRaNetModelData.h"

quint32 CRaNetModelData::fPacketFreq = 0;
//-----------------------------------------------------------------------------
CRaNetModelData::CRaNetModelData(quint16 aIndex,quint16 aport,QObject *parent) :
    QObject(parent),fIndex(aIndex)
{
    fMemory = NULL;
    fSockect = new QUdpSocket();
    fSockect->bind(aport,QAbstractSocket::ReuseAddressHint & QAbstractSocket::ShareAddress);
    connect(fSockect,SIGNAL(readyRead()),this,SLOT(onDataReceive()));


    fUpdateTimer = new QTimer();
    //---
    fUpdateTimer->setInterval(50);
    connect(fUpdateTimer,SIGNAL(timeout()),this,SLOT(UpdateDataModelTimer()),Qt::QueuedConnection);
    fUpdateTimer->start();

}
//-----------------------------------------------------------------------------
CRaNetModelData::~CRaNetModelData(){
    delete fSockect;
    fUpdateTimer->stop();
    delete fUpdateTimer;
}
//-----------------------------------------------------------------------------
void    CRaNetModelData::onDataReceive(){
    fPacketFreq = fTimer.restart();

    if(fSockect!=NULL && fMemory!=NULL){
        while (fSockect->hasPendingDatagrams()) {
            quint32 lsize = fSockect->pendingDatagramSize();
            fSockect->readDatagram((char*)fMemory, lsize);
        }
    }
}
//-----------------------------------------------------------------------------
void    CRaNetModelData::UpdateDataModelTimer(){
    if(fMemory!=NULL){
        emit DataReceive();
    }
}
